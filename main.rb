require_relative 'utility'

handlelog = HandleLogFileUtility.new()

handlelog.process_analysis_log()
handlelog.print_url_call_count()
handlelog.print_analysis_time()
handlelog.print_most_dyno_count()
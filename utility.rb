

class HandleLogFileUtility

 	def initialize(sample_file="sample.log")
 		@sample_file = sample_file
 		@post_url_call = 0
 		@get_count_pending_messages_url_call = 0
 		@get_get_messages_url_call = 0
 		@get_get_friends_score_url_call = 0
 		@get_get_friends_progess_url_call = 0
 		@get_url_call = 0
 		@mean_response = 0
 		@median_response = 0
 		@mode_response = []
 		@most_dyno = []
 		@response_time_arr = []
 		@dyno_arr = []
 	end

 	def get_post_url_call()
 		return @post_url_call
 	end

 	def get_count_pending_messages_url_call()
 		return @get_count_pending_messages_url_call
 	end

 	def get_get_messages_url_call()
 		return @get_get_messages_url_call
 	end

 	def get_get_friends_score_url_call()
 		return @get_get_friends_score_url_call
 	end

  	def get_get_friends_progess_url_call()
 		return @get_get_friends_progess_url_call
 	end

   	def get_get_url_call()
 		return @get_url_call
 	end

 	def get_most_dyno()
 		return @most_dyno
 	end

 	def process_analysis_log()
 		begin
		    file = File.new(@sample_file, "r")
		    while (line = file.gets)
		        self._analysis_log(line)
		    end
		    file.close

		    self._analysis_time()
		    self._find_most_dyno()
		rescue => exception
		    puts "Exception: #{exception}"
		    exception
		end
 	end

 	def _find_mode(array)
	    count = []
	    output = [] 
	    result = []
	    array.compact!
	    unique = array.uniq
	    j=0

	    unique.each do |i|
	        count[j] = array.count(i)
	        j+=1
	    end
	    k=0
	    count.each do |i|
	        output[k] = unique[k] if i == count.max
	        k+=1
	    end

	    result = output.compact
	    if result.length == array.length
	    	result = []
	    end
	    return result
	end

	def _find_mean(array)
		mean = 0
 		for response in array
 			mean += response
 		end
 		return mean.to_f() / array.length
	end

	def _find_median(array)
		median = 0
 		# Check length is even or odd
 		if array.length % 2 == 0
 			# handle even
 			left_value = array.at((array.length / 2) - 1)
 			right_value = array.at(array.length / 2)
 			median = (left_value + right_value).to_f() / 2
 		else
 			# handle odd
 			index_of_median = (array.length + 1) / 2
 			median = array.at(index_of_median - 1).to_f()
 		end
 		return median
	end

 	def _analysis_time()
 		mode = "None"
 		mean = _find_mean(@response_time_arr)
 		median = _find_median(@response_time_arr)

 		mode_arr = self._find_mode(@response_time_arr)
 		if mode_arr.any?
 			mode = mode_arr
 		end

 		@mean_response = mean.round(2)
 		@median_response = median.round(2)
 		@mode_response = mode
 		
 	end

 	def _find_most_dyno()
 		most_dyno = "None"

 		most_dyno_arr = self._find_mode(@dyno_arr)
 		if most_dyno_arr.any?
 			most_dyno = most_dyno_arr
 		end

 		@most_dyno = most_dyno
 	end

 	def _analysis_log(body)
    	method = body.scan(/method=([^>]\S*)/)
    	is_valid_format = true
    	if method.any?
    		method = method.first.first.downcase
    	else
    		is_valid_format = false
    	end

    	path = body.scan(/path=([^>]\S*)/)
    	if path.any?
    		path = path.first.first
    	else
    		is_valid_format = false
    	end

    	service = body.scan(/service=([^>]*)ms\S*/)
    	if service.any?
    		service = service.first.first
    	else
    		is_valid_format = false
    	end

    	connect = body.scan(/connect=([^>]*)ms\S*/)
    	if connect.any?
    		connect = connect.first.first
    	else
    		is_valid_format = false
    	end

    	dyno = body.scan(/dyno=([^>]\S*)/)
    	if dyno.any?
    		dyno = dyno.first.first
    	else
    		is_valid_format = false
    	end

    	#user = path.scan(/\/api\/users\/([^\/]*)/) #if you want to show what user has call

    	if is_valid_format
    		@response_time_arr.push(connect.to_i() + service.to_i())
    		@dyno_arr.push(dyno)

	 		if method == "post"
	   			post_url_scan = path.scan(/(^\/api\/users\/[^\/]*)/)
	   			if post_url_scan.any?
		   			if path == post_url_scan.first.first
		   				@post_url_call += 1
		   			end
	   			end
	   		elsif method == "get"
	   			get_count_pending_scan = path.scan(/(^\/api\/users\/[^>]*\/count_pending_messages[^\/]*)/)
	   			get_msg_scan = path.scan(/(^\/api\/users\/[^>]*\/get_messages[^\/]*)/)
	   			get_friends_progress_scan = path.scan(/(^\/api\/users\/[^>]*\/get_friends_progress[^\/]*)/)
	   			get_friends_score_scan = path.scan(/(^\/api\/users\/[^>]*\/get_friends_score[^\/]*)/)
	   			get_url_scan = path.scan(/(^\/api\/users\/[^\/]*)/)

	   			if get_count_pending_scan.any?
		   			if path == get_count_pending_scan.first.first
		   				@get_count_pending_messages_url_call += 1
		   			end
		   		elsif get_msg_scan.any?
		   			if path == get_msg_scan.first.first
		   				@get_get_messages_url_call += 1
		   			end
		   		elsif get_friends_progress_scan.any?
		   			if path == get_friends_progress_scan.first.first
		   				@get_get_friends_progess_url_call += 1
		   			end
		   		elsif get_friends_score_scan.any?
		   			if path == get_friends_score_scan.first.first
		   				@get_get_friends_score_url_call += 1
		   			end
		   		elsif get_url_scan.any?
		   			if path == get_url_scan.first.first
		   				@get_url_call += 1
		   			end
	   			end
	   		else
	  			# Not handle other kind of method
			end
		end
 	end

 	def print_analysis_time()
 		puts("------analysis time section!-------")
 		puts("Mean(Average) response time is #{@mean_response} ms\n")
 		puts("Median response time is #{@median_response} ms\n")
 		puts("Mode response time is #{@mode_response} ms\n")
 		puts("-----------------------------------")
 	end

 	def print_most_dyno_count()
 		puts("------Additional Info!-------")
 		puts("Dyno that responded the most : #{@most_dyno}\n")
 		puts("-----------------------------------")
 	end

 	def print_url_call_count()
 		puts("--number of url was called section!--")
 		puts("POST /api/user/{user_id}/ was call #{@post_url_call} times\n")
 		puts("GET /api/users/{user_id}/count_pending_messages was call #{@get_count_pending_messages_url_call} times\n")
 		puts("GET /api/users/{user_id}/get_messages was call #{@get_get_messages_url_call} times\n")
 		puts("GET /api/users/{user_id}/get_friends_progress was call #{@get_get_friends_progess_url_call} times\n")
 		puts("GET /api/users/{user_id}/get_friends_score was call #{@get_get_friends_score_url_call} times\n")
 		puts("GET /api/users/{user_id} was call #{@get_url_call} times\n")
 		puts("-----------------------------------")
 	end
end
require "test/unit"
require_relative 'utility'

class TestLoggingCurl < Test::Unit::TestCase

	def setup

		@post_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=POST path=/api/users/1145906359"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.13 connect=2ms service=362ms status=200 bytes=52"

		@get_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/api/users/1145906359"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@get_friends_score_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/api/users/1145906359/get_friends_score"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@get_friends_progress_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/api/users/1145906359/get_friends_progress"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@get_friends_messages_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/api/users/1145906359/get_messages"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@get_count_pending_messages_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/api/users/1145906359/count_pending_messages"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@post_invalid_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/testinvalid/blabla"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@get_invalid_url = "2014-01-09T06:15:17.858874+00:00 heroku[router]:"\
		+ "at=info method=GET path=/testinvalid/blabla"\
		+ "host=services.pocketplaylab.com fwd=\"107.220.72.53\""\
		+ "dyno=web.14 connect=2ms service=362ms status=200 bytes=52"

		@handlelog = HandleLogFileUtility.new()
	end

	def test_analysis_log_post_url
		@handlelog._analysis_log(@post_url)
		result = @handlelog.get_post_url_call()
		assert_equal(result,1)
	end 

	def test_analysis_log_get_url
		@handlelog._analysis_log(@get_url)
		result = @handlelog.get_get_url_call()
		assert_equal(result,1)
	end 

	def test_analysis_log_get_friends_score_url
		@handlelog._analysis_log(@get_friends_score_url)
		result = @handlelog.get_get_friends_score_url_call()
		assert_equal(result,1)
	end 

	def test_analysis_log_get_friends_progress_url
		@handlelog._analysis_log(@get_friends_progress_url)
		result = @handlelog.get_get_friends_progess_url_call()
		assert_equal(result,1)
	end 

	def test_analysis_log_get_messages_url
		@handlelog._analysis_log(@get_friends_messages_url)
		result = @handlelog.get_get_messages_url_call()
		assert_equal(result,1)
	end 

	def test_analysis_log_get_count_pending_messages_url
		@handlelog._analysis_log(@get_count_pending_messages_url)
		result = @handlelog.get_count_pending_messages_url_call()
		assert_equal(result,1)
	end 

	def test_analysis_log_invalid_get_url
		@handlelog._analysis_log(@get_invalid_url)
		result = @handlelog.get_get_url_call()
		assert_equal(result,0)
	end 

	def test_analysis_log_invalid_post_url
		@handlelog._analysis_log(@post_invalid_url)
		result = @handlelog.get_post_url_call()
		assert_equal(result,0)
	end 

	def test_find_mode_none
		array = [1,2,3,4]
		result = @handlelog._find_mode(array)
		assert_equal(result, [])
	end

	def test_find_mode_number
		array = [1,1,3,4]
		result = @handlelog._find_mode(array)
		assert_equal(result, [1])
	end

	def test_find_mode_character
		array = ["test","test","55","66"]
		result = @handlelog._find_mode(array)
		assert_equal(result, ["test"])
	end

	def test_find_mode_combine
		array = ["test","test",1,2]
		result = @handlelog._find_mode(array)
		assert_equal(result, ["test"])
	end

	def test_find_median_even
		array = [1,2,3,4]
		median = 2.5
		result = @handlelog._find_median(array)
		assert_equal(result, median)
	end

	def test_find_median_odd
		array = [1,2,3,4,5]
		median = 3
		result = @handlelog._find_median(array)
		assert_equal(result, median)
	end

	def test_find_mean
		array = [1,2,3,4,5]
		mean = 3
		result = @handlelog._find_mean(array)
		assert_equal(result, mean)
	end

	def test_find_most_dyno
		@handlelog._analysis_log(@get_friends_progress_url)
		@handlelog._analysis_log(@get_friends_score_url)
		@handlelog._analysis_log(@post_url)
		@handlelog._find_most_dyno()
		result = @handlelog.get_most_dyno()
		assert_equal(result, ["web.14"])
	end

	def test_find_most_dyno_none
		@handlelog._analysis_log(@get_friends_progress_url)
		@handlelog._analysis_log(@post_url)
		@handlelog._find_most_dyno()
		result = @handlelog.get_most_dyno()
		assert_equal(result, "None")
	end
end